﻿using Nomina.Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class Colilla : Form
    {
        private DataTable tblEmpleado;
        private DataSet dsEmpleado;
        private BindingSource bsEmpleado;
        private DataRow drEmpleado;

        public Colilla()
        {
            InitializeComponent();
            bsEmpleado = new BindingSource();
        }

        public DataTable TblProductos { set { tblEmpleado = value; } }
        public DataSet DsEmpleado { set { dsEmpleado = value; } }

        public DataRow DrEmpleado
        {
            set
            {
                drEmpleado = value;
                txtNombre.Text = drEmpleado["Nombre"].ToString();
                txtApellido.Text = drEmpleado["Apellidos"].ToString();
            }
        }

        public void setEmpleado(Empleado e, int index)
        {
            txtNombre.Text = e.Nombres;
            txtApellido.Text = e.Apellidos;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Colilla_Load(object sender, EventArgs e)
        {
            bsEmpleado.DataSource = dsEmpleado;
            bsEmpleado.DataMember = dsEmpleado.Tables["Empleado"].TableName;
        }


    }
}
