﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Business.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        private DataTable dtEmpleado;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadoImplements.All();

            empleados.ForEach(ep => {
                dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            });

            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;

            dtEmpleado = dsNomina.Tables["Empleado"];
            EmpleadoModel.Populate();

            foreach(Empleado em in EmpleadoModel.GetLstEmpleado())
            {
                dtEmpleado.Rows.Add(em.Id, em.Cedula, em.Nombres, em.Apellidos, em.Direccion, em.Telefono, em.FechaContratacion, em.Salario);
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DSNomina = dsNomina;
            frmEmpleado.ShowDialog(this);
        }

        string idSleccionado;
        string IdSeleccionado
        {
            get { return idSleccionado; }
            set { idSleccionado = value; }
        }

        private void VerColilla(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            Colilla cl = new Colilla();
            cl.txtNombre.Text = this.dgvEmpleados.CurrentRow.Cells[2].Value.ToString();
            cl.txtApellido.Text = this.dgvEmpleados.CurrentRow.Cells[3].Value.ToString();
            cl.txtSalario.Text = this.dgvEmpleados.CurrentRow.Cells[7].Value.ToString();
            cl.TblProductos = dsNomina.Tables["Empleado"];
            cl.DsEmpleado = dsNomina;            
            cl.ShowDialog();
        }
        

        private void dgvEmpleados_MouseClick(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {
                var hti = dgvEmpleados.HitTest(e.X, e.Y);
                dgvEmpleados.ClearSelection();
                dgvEmpleados.Rows[hti.RowIndex].Selected = true;

                IdSeleccionado = dgvEmpleados.Rows[hti.RowIndex].Cells[0].Value.ToString();

                ContextMenu c = new ContextMenu();

                MenuItem mi = new MenuItem();

                mi.Text = "Visualizar Colilla";
                mi.Click += VerColilla;
                c.MenuItems.Add(mi);

                c.Show(dgvEmpleados, new Point(e.X, e.Y));
            }

            
        }        

        private void visuaToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
