﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Nomina.Business.Dao;
using Nomina.Business.Entities;
using Nomina.Business.Utilities;

namespace Nomina.Business.Implements
{
    public class DaoEmpleadoImplements : IDaoEmpleado
    {
        private BinaryWriter bwHeader;
        private BinaryWriter bwData;
        private BinaryReader brHeader;
        private BinaryReader brData;

        private FileStream fsHeader;
        private FileStream fsData;

        private const int SIZE = 980;

        private List<HeaderIndex> headerIndices;
        private Header header;
        public DaoEmpleadoImplements() { }

        private void Open()
        {
            try
            {
                headerIndices = new List<HeaderIndex>()
                {
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "IdEmpleado"
                    },
                    new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "CedulaEmpleado"
                    },
                     new HeaderIndex()
                    {
                        N = 0,
                        K = 0,
                        NameHeaderIndex = "ApellidosEmpleado"
                    }
                };

                header = new Header()
                {
                    N = 0,
                    Name = "hEmpleado",
                    HeaderIndices = headerIndices
                };


                fsHeader = new FileStream("hempleado.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                fsData = new FileStream("dempleado.data",FileMode.OpenOrCreate, FileAccess.ReadWrite);
                bwHeader = new BinaryWriter(fsHeader);
                brHeader = new BinaryReader(fsHeader);

                bwData = new BinaryWriter(fsData);
                brData = new BinaryReader(fsData);
                if(fsHeader.Length == 0){
                    bwHeader.Write(0);//n
                    bwHeader.Write(0);//k
                }

            }catch(IOException ex)
            {
                throw ex;
            }
        }

        private void Close()
        {
            try
            {
                if (bwData != null)
                {
                    bwData.Close();
                }
                if (bwHeader != null)
                {
                    bwHeader.Close();
                }
                if (brData != null)
                {
                    brData.Close();
                }
                if (brHeader != null)
                {
                    brHeader.Close();
                }
                if (fsData != null)
                {
                    fsData.Close();
                }
                if (fsHeader != null)
                {
                    fsHeader.Close();
                }
            }
            catch (IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public Empleado findById(int id)
        {
            Empleado empleado = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                int index = Finder.BinarySearchById(brHeader, id, 0, n - 1);

                if(index < 0)
                {
                    return empleado;
                }

                long dpos = index * SIZE;
                brData.BaseStream.Seek(dpos, SeekOrigin.Begin);
                empleado = new Empleado()
                {
                    Id = brData.ReadInt32(),
                    Cedula = brData.ReadString(),
                    Nombres = brData.ReadString(),
                    Apellidos = brData.ReadString(),
                    Direccion = brData.ReadString(),
                    Telefono = brData.ReadString(),
                    //FechaContratacion = new DateTime(brData.ReadInt64(), DateTimeKind.Local),
                    Salario = brData.ReadDecimal()
                };

                Close();
            }
            catch(IOException ex)
            {
                throw ex;
            }
            return empleado;
        }

        public Empleado findByCedula(string cedula)
        {
            Empleado empleado = null;
            try
            {
                Open();
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    int dId = brData.ReadInt32();
                    string dCedula = brData.ReadString();

                    if (!cedula.Equals(dCedula, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }
                    empleado = new Empleado()
                    {
                        Id = dId,
                        Cedula = dCedula,
                        Nombres = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Direccion = brData.ReadString(),
                        Telefono = brData.ReadString(),
                       // FechaContratacion = new DateTime(brData.ReadInt64(), DateTimeKind.Local),
                        Salario = brData.ReadDecimal()
                    };
                    break;                   
                }


                Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
            return empleado;
        }

        public void Create(Empleado t)
        {
            try
            {
                Open();

                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();

                long pos = k * SIZE;
                bwData.BaseStream.Seek(pos, SeekOrigin.Begin);

                bwData.Write(++k);
                bwData.Write(t.Cedula);
                bwData.Write(t.Nombres);
                bwData.Write(t.Apellidos);
                bwData.Write(t.Direccion);
                bwData.Write(t.Telefono);
               // bwData.Write(t.FechaContratacion.Ticks);
                bwData.Write(t.Salario);

                bwHeader.BaseStream.Seek(0, SeekOrigin.Begin);
                bwHeader.Write(++n);
                bwHeader.Write(k);

                long hpos = 8 + (n - 1) * 4;
                bwHeader.BaseStream.Seek(hpos,SeekOrigin.Begin);
                bwHeader.Write(k);

                Close();
            }
            catch(IOException ex)
            {
                throw new IOException(ex.Message);
            }
        }

        public bool Update(Empleado t)
        {
            throw new NotImplementedException();
        }

        public bool Delete(Empleado t)
        {
            throw new NotImplementedException();
        }

        public List<Empleado> All()
        {
            List<Empleado> empleados = new List<Empleado>();
            try
            {
                Open();
               
                brHeader.BaseStream.Seek(0, SeekOrigin.Begin);

                int n = brHeader.ReadInt32();
                int k = brHeader.ReadInt32();
                Empleado empleado = null;
                for (int i = 0; i < n; i++)
                {
                    long hpos = 8 + i * 4;
                    brHeader.BaseStream.Seek(hpos, SeekOrigin.Begin);

                    int index = brHeader.ReadInt32();

                    long dpos = (index - 1) * SIZE;
                    brData.BaseStream.Seek(dpos, SeekOrigin.Begin);

                    empleado = new Empleado()
                    {
                        Id = brData.ReadInt32(),
                        Cedula = brData.ReadString(),
                        Nombres = brData.ReadString(),
                        Apellidos = brData.ReadString(),
                        Direccion = brData.ReadString(),
                        Telefono = brData.ReadString(),
                        //FechaContratacion = new DateTime(brData.ReadInt64(), DateTimeKind.Local),
                        Salario = brData.ReadDecimal()
                    };

                    empleados.Add(empleado);
                }
                Close();
            }
            catch(IOException ex)
            {
                throw ex;
            }
            
            return empleados;
        }
    }
}
