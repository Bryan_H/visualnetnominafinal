﻿using Nancy.Json;
using Nomina.Business.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nomina.Business.Model
{
    public class EmpleadoModel
    {
        private static List<Empleado> lstEmpleado = new List<Empleado>();

        public static List<Empleado> GetLstEmpleado()
        {
            return lstEmpleado;
        }

        public static void Populate()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            lstEmpleado = serializer.Deserialize<List<Empleado>>(System.Text.Encoding.Default.GetString(Nomina.Business.Properties.Resources.EmpleadoData));
        }
    }
}
